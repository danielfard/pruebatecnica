from django.contrib import admin
from task.models import Task, UserProfile

# Register your models here.

admin.site.register(UserProfile)
admin.site.register(Task)
