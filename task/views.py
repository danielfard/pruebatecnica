from task.serializers import TaskSerializer, UserProfileSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser
from task.models import Task, UserProfile
from rest_framework import status
from datetime import datetime

class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    def get_queryset(self, pk=None):
        """
        this get method brings me to a specific task
        it responds with a status code 200, in case it fails it returns a 424
        """
        
        if pk is None:
            return self.serializer_class.Meta.model.objects.filter(state=True)

        if Task.objects.filter(id=pk).first():
            return self.serializer_class.Meta.model.objects.get(id=pk)
        else:
            return None

    def list(self, request):
        """
        this get method brings me all the created tasks
        it responds with a status code 200, in case it fails it returns a 424
        """
        try:
            tasks = Task.objects.all().values()
            return Response(data=tasks, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )

    def retrieve(self, request, pk=None):
        """
        this get method brings me a task
        it responds with a status code 200, in case it fails it returns a 424
        """
        try:
            
            task = self.get_queryset(pk)
            if task:
                return Response(data=TaskSerializer(task).data, status=status.HTTP_200_OK)
            return Response(data={"message": "Task doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )

    def create(self, request):
        """
        this method creates a task, if everything is executed correctly it returns the created object and a status code of 200
        otherwise it will execute a 424 dependency failure.
        """
        try:
            data = request.data
            initial_date = datetime.strptime(data["initial_date"], '%Y-%m-%dT%H:%M')
            finalized_date = datetime.strptime(data["finalized_date"], '%Y-%m-%dT%H:%M')
            if finalized_date < initial_date:
                return Response(
                    data={"message": "the end date is less than the start date"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            task = Task(
                title=data["title"],
                description=data["description"],
                state=data["state"],
                finalized_date=data["finalized_date"],
                initial_date=data["initial_date"],
            )

            task.save()
            user = UserProfile.objects.get(id=data["appointees"])
            task.appointees.add(user)

            return Response(data=TaskSerializer(task).data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )

    def update(self, request, partial=None,pk=None):
        """
        this method updates a task from its PK and information,
        if it executes correctly we return the updated object and a status code of 200 otherwise we return a 424.
        """
        try:
            if self.get_queryset(pk):
                task = self.get_queryset(pk)

                serializer = self.serializer_class(task,data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(
                        data=serializer.data,status=status.HTTP_200_OK
                    )
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(
                    data={"message": "Task doesn't exist"}, status=status.HTTP_404_NOT_FOUND
                )
            

        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )

    def destroy(self, request, pk=None):
        """
        this method deletes an object of type task from the database,
        it responds a status code of 200 if the object is deleted otherwise it responds a 424.
        """
        try:
            if self.get_queryset(pk):
                self.perform_destroy(self.get_queryset(pk))
                return Response(
                    data={"message": "task deleted"}, status=status.HTTP_200_OK
                )
            return Response(
                data={"message": "Task doesn't exist"}, status=status.HTTP_404_NOT_FOUND
            )
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )


class UserViewSet(viewsets.ViewSet):
    serializer_class = UserProfileSerializer

    def list(self, request):
        """
        it lists all the users created in the database and returns them,
        otherwise it would return the error and an exception 424
        """
        try:
            users = UserProfile.objects.all().values()
            return Response(data=users, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )

    def create(self, request):
        """
        it creates a user with the user's name,
        if everything goes well it returns the name of the created user and a status code 201,
        otherwise it returns 424.
        """
        try:
            data = request.data
            user = UserProfile(
                name=data["name"],
            )
            user.save()
            return Response(data={"name": user.name}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response(
                data={"message": f"error: {e.args[0]}"},
                status=status.HTTP_424_FAILED_DEPENDENCY,
            )
