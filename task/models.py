from django.db import models
import uuid

# Create your models here.
GENRES = (
    ("para hacer", "para hacer"),
    ("en progreso", "en progreso"),
    ("completada", "completada"),
)


class UserProfile(models.Model):
    """
    we create a model of type user where we assign the following attributes.
        - document: UUID
        - name: STR
    this model is the user that is linked with the assigned task
    """

    document = models.SlugField(unique=True, default=uuid.uuid1)
    name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.name


class Task(models.Model):
    """
    we create a task type model where we assign the following attributes.
        - title: UUID
        - description: STR
        - state: str
        - appointees: user[]
        - finalized_date: Date
        - initial_date: Date
    this model has a many-to-many relationship with user to assign multiple users to a certain task
    """

    title = models.CharField(max_length=200)
    description = models.TextField()
    state = models.CharField(choices=GENRES, max_length=50)
    appointees = models.ManyToManyField(UserProfile)
    finalized_date = models.DateTimeField(blank=False, null=False)
    initial_date = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return self.title
