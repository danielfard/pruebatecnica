from email.policy import default
from rest_framework import serializers
from task.models import Task, UserProfile, GENRES

# Serializers define the API representation.

class UserProfileSerializer(serializers.ModelSerializer):
    name=serializers.CharField(required=False)
    class Meta:
        model = UserProfile
        fields = ("name",)
        depth = 1

class TaskSerializer(serializers.ModelSerializer):
    title=serializers.CharField(required=False)
    description=serializers.CharField(required=False)
    state=serializers.ChoiceField(GENRES,required=False)
    appointees= UserProfileSerializer(many=True, required=False, read_only =True)
    finalized_date=serializers.DateTimeField(required=False)
    initial_date=serializers.DateTimeField(required=False)
    class Meta:
        model = Task
        fields = [
            "title",
            "description",
            "state",
            "appointees",
            "finalized_date",
            "initial_date",
        ]
    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep["appointees"] = UserProfileSerializer(instance.appointees.all(), many=True).data
        return rep



    
